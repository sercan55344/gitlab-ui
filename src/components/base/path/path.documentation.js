import description from './path.md';

export default {
  description,
  followsDesignSystem: true,
};
